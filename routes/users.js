var express = require('express');
var router = express.Router();
var md5 = require('MD5');
var async = require('async');
var func = require('./commonFunction');
var sendResponse = require('./sendResponse');

/*
 * --------------------------------------------------------------------------
 * sign_up
 * INPUT : firstName, lastName, email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/sign_up', function (req, res, next) {

    var firstName = req.body.first_name;
    var lastName = req.body.last_name;
    var email = req.body.email;
    var password = req.body.password;
    var manValues = [firstName, lastName, email, password];
    async.waterfall([
        function (callback) {
            func.checkBlank(res, manValues, callback);
        },
        function (callback) {
            func.checkEmailAvailability(res, email, callback);
        }],
            function (resultCallback) {
                var loginTime = new Date();
                var accessToken = func.encrypt(email + loginTime);
                var encryptPassword = md5(password);
                var sql = "INSERT into users(email,password,access_token,last_login,first_name,last_name)values(?,?,?,?,?,?)";
                var values = [email, encryptPassword, accessToken, loginTime, firstName, lastName];

                connection.query(sql, values, function (err, userInsertResult) {
                    if (err) {

                        sendResponse.somethingWentWrongError(res);
                    }
                    else {
                        var data = {
                            access_token: accessToken,
                            first_name: firstName,
                            last_name: lastName
                        };
                        sendResponse.sendSuccessData(data, res);
                    }
                });

            }

    );
});

/*
 * --------------------------------------------------------------------------
 * login_user
 * INPUT : email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/login_user', function (req, res, next) {
    var email = req.body.email;
    var password = req.body.password;
    var checkBlank = [email, password];
    var accessToken = "SELECT 'access_token' FROM users";
    async.waterfall(
            [function (callback) {
                    func.checkBlank(res, checkBlank, callback);
                }, function (callback) {
                    console.log("inside here");
                    func.checkEmail(res, email, callback);
                },
                function (callback) {
                    func.accessTokenCheck(accessToken, res, callback);
                }],
            function (resultCallback) {

                var loginTime = new Date();
                var accessToken = func.encrypt(email + loginTime);
                var loginStatus = 1;
                var sql = "INSERT into users(access_token,last_login, login_status)values(?,?,?)";
                var values = [accessToken, loginTime, loginStatus];

                connection.query(sql, values, function (err, userInsertResult) {
                    if (err) {
                        console.log(err);
                        res.send("ERR");
                    }
                    else {

                        sendResponse.sendSuccessData("LOGGED IN", res);
                    }

                });

            });
});

/*
 * --------------------------------------------------------------------------
 * logout
 * INPUT : email, password
 * OUTPUT : error, userResult
 * ---------------------------------------------------------------------------
 */
router.post('/logout', function (req, res, next) { 
    var loginStatus = 0;
    var userId = req.body.user_id;
    console.log(userId);
    var accessToken = req.body.access_token;
    console.log(accessToken);
    func.accessTokenCheck(accessToken, res,function(callback)
    {
            var sql = "UPDATE users SET login_status=? WHERE user_id=? LIMIT 1";
            var values = [loginStatus, userId];
            console.log(values);
            connection.query(sql, values, function (err, userInsertResult) {
                if (err) {
                    
                    sendResponse.somethingWentWrongError(res);
                }
                else {
                    //console.log(userInsertResult);
                   sendResponse.logoutSuccess(constant.responseMessage.LOGOUT_SUCCESSFULL, res);
                }
            });
       
    });
      
});
module.exports = router;