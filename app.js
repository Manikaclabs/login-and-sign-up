

process.env.NODE_ENV = 'test';
config = require('config');
dbConnection = require('./routes/dbconnection');
constant = require('./routes/constant');


var express = require('express');


var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');

var app = express();


var routes = require('./routes/index');
var users = require('./routes/users');



// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.get('/test', routes);
app.get('/', routes);
app.post('/sign_up', users);
app.post('/login_user', users);
app.post('/logout', users);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});

//module.exports = app;
